# Project Title

Perform analysis on crypto currency using price and telegram chat sentiment data 

## Getting Started

Just Open the notebook and Run all cells
In options of notebook
  cell -> run all

### Prerequisites

 - Python 3
 - Jupyter Notebook 


### Running the notebook 

Just run ```jupyter notebook```